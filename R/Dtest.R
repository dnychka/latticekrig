


Dtest<- function(x1, n1=nrow(x1), nGrid=c( 15,20), nDim=length(nGrid), delta=(2.2),
Nmax)
# ind, ra, Nmax, iflag) 
{	
gridStep <- cM<- M <- m1<- rep( NA, nDim)
delta2   <- ( delta)^2
deltaX   <- rep( NA, nDim)
ra       <- rep( NA, Nmax)
ind      <- matrix( NA, Nmax, 2)
#
gridStep[1]<- 1
for( j in 2:nDim){
	gridStep[j]<- gridStep[j-1]* nGrid[j-1]
}
  kk<- 0
for(  i in 1:n1){
  prodM <- 1
  offset <- 0 
  for( j in (1:nDim) ){
    m1  <- max( ceiling( -delta + x1[i,j]), 1       )
    m2 <- min( floor(    delta + x1[i,j]), nGrid[j]) 
# jth dimension has grid points within delta indexed 
#  by  m1 ... m2  there are M[j] points in this set  
    M[j] <- ( m2 - m1 ) + 1
    prodM<- prodM*(M[j])
    deltaX[j]<- x1[i,j] - m1
    offset <- offset + ( m1 - 1  )*gridStep[j]
  }
  cM[1]<- 1
  for( j in (2:nDim) ){
    cM[j]<- cM[j-1]*( M[j-1])
  } 
  for( k in 1: (prodM) ){	
	dist2Temp<- 0
	L<- k - 1 
	indTemp<- 0   
    for( j in (nDim:1) ){
    	kI<- floor( L/(cM[j]) ) 
    	L <- L - kI*cM[j]	
    	indTemp<- indTemp + ( kI )*gridStep[j]
    	dist2Temp<- dist2Temp + (deltaX[j] - kI)^2
    }   	
    if( dist2Temp<= delta2){	
        	kk<- kk +1
            ra[kk]<- sqrt( dist2Temp)
            ind[kk,2]<- indTemp	+ 1 + offset 
            ind[kk, 1] <- i
         }
   }
Nmax<- kk
}
return(list( ra= ra[1:Nmax], ind= ind[1:Nmax,],
         da= c(n1,prod(nGrid)),  Nmax=Nmax) )
}